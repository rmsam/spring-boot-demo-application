This is a test application for nerd.vision using spring boot and hibernate

To configure:
export MAVEN_OPTS="-javaagent:/{path}/nerdvision.jar=api.key=xxxx"

To run:
mvn spring-boot:run

Application users:
admin - admin1234
user1 - admin1234
user2 - admin1234