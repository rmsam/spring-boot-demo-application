<head>
   <!-- Latest compiled and minified bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <link href="${contextPath}/resources/css/common.css" rel="stylesheet" type = "text/css">
</head>

<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<nav class="navbar navbar-default">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#">Shop Example</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li>
            <form:form method = "GET" action = "/shop">
              <table>
                  <tr>
                      <td>
                          <input class="header-button" type = "submit" value = "Shop"/>
                      </td>
                  </tr>
              </table>
            </form:form>
        </li>
        </li>
        <li>
          <form:form method = "GET" action = "/cart">
              <table>
                  <tr>
                      <td>
                          <input class="header-button" type = "submit" value = "View Cart"/>
                      </td>
                  </tr>
              </table>
          </form:form>
        </li>
        <li>
          <form:form method = "GET" action = "/admin">
              <table>
                  <tr>
                      <td>
                          <input class="header-button" type = "submit" value = "Admin Panel"/>
                      </td>
                  </tr>
              </table>
          </form:form>
        </li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li>
            <h4>Welcome ${pageContext.request.userPrincipal.name} | <a onclick="document.forms['logoutForm'].submit()">Logout</a></h4>
        </li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>

<div class="container">
    <c:if test="${pageContext.request.userPrincipal.name != null}">
        <form id="logoutForm" method="POST" action="${contextPath}/logout">
            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
        </form>
    </c:if>
</div>