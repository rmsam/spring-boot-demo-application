<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Cart</title>

    <link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/common.css" rel="stylesheet" type = "text/css">
</head>

<body>
<jsp:include page="header.jsp" />

<b>Todays deal: 50% off all SSDs</b>

<div>
    <table>
        <tr>
            <th>Product Name</th>
            <th>Product Description</th>
            <th>Product Price</th>
            <th>Product Quantity</th>
            <th>Actions</th>
        </tr>
        <c:forEach var="cart" items="${carts}">
            <tr>
                <td>"${cart.product.name}"</td>
                <td>"${cart.product.description}"</td>
                <td>"${cart.product.price}"</td>
                <td>"${cart.quantity}"</td>
                <td>
                    <form:form method = "GET" action = "/cart/${cart.product.id}/add">
                        <table>
                            <tr>
                                <td>
                                    <input type = "submit" value = "Add 1 item"/>
                                </td>
                            </tr>
                        </table>
                    </form:form>
                    <form:form method = "GET" action = "/cart/${cart.product.id}/remove">
                        <table>
                            <tr>
                                <td>
                                    <input type = "submit" value = "Remove 1 item"/>
                                </td>
                            </tr>
                        </table>
                    </form:form>
                    <form:form method = "DELETE" action = "/cart/${cart.product.id}">
                        <table>
                            <tr>
                                <td>
                                    <input type = "submit" value = "Remove product"/>
                                </td>
                            </tr>
                        </table>
                    </form:form>
                </td>
            </tr>
        </c:forEach>
    </table>
    <h1>Total (Broken - No book discount applied) : ${total}</h1>
    <form:form method = "GET" action = "/cart/buynow">
        <table>
            <tr>
                <td>
                    <input type = "submit" value = "Buy Now"/>
                </td>
            </tr>
        </table>
    </form:form>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap.min.js"></script>
</body>
</html>