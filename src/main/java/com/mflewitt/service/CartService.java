package com.mflewitt.service;

import com.mflewitt.model.Cart;
import com.mflewitt.model.Product;
import com.mflewitt.model.User;
import java.math.BigDecimal;
import java.util.List;

public interface CartService
{
    void addProduct( User user, Product product );
    void removeProduct( User user, Product product );
    void increaseQuantity( User user, Product product );
    void decreaseQuality( User user, Product product );
    void buyNow(User user);
    BigDecimal getTotal(User user);
    List<Cart> getProductsinCart( User user);
}
