package com.mflewitt.service.impl;

import com.mflewitt.model.Role;
import com.mflewitt.repository.RoleRepository;
import com.mflewitt.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RoleServiceImpl implements RoleService
{
    @Autowired
    private RoleRepository roleRepository;

    @Override
    public void save( final Role role )
    {
        roleRepository.save( role );
    }


    @Override
    public Role findByName( final String name )
    {
        return roleRepository.findByName( name );
    }
}
