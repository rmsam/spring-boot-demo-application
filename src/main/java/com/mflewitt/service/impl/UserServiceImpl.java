package com.mflewitt.service.impl;

import com.mflewitt.model.Role;
import com.mflewitt.model.User;
import com.mflewitt.repository.RoleRepository;
import com.mflewitt.repository.UserRepository;
import com.mflewitt.service.UserService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService
{
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Override
    public void save( User user) {
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        user.setRole(roleRepository.findByName( "customer" ) );
        userRepository.save(user);
    }


    @Override
    public User findById( final Long id )
    {
        return userRepository.getOne( id );
    }


    @Override
    public User findByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    @Override
    public User searchName( final String name )
    {
        return userRepository.findByUsernameIsLike( name );
    }


    @Override
    public void makeAdmin(Long id)
    {
        User user = userRepository.getOne( id );
        Role role = roleRepository.findByName( "administrator" );
        int i = 10;
        if(role.getUsers().size() < 10)
        {
            user.setRole( role );

        }
        System.out.println(i);
        userRepository.save( user );
    }


    @Override
    public List<User> findAll()
    {
        return userRepository.findAll();
    }
}