package com.mflewitt.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="cart")
public class Cart
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    private User user;

    @ManyToOne
    private Product product;

    private Integer quantity;

    public Integer getQuantity()
    {
        return quantity;
    }

    public void setQuantity( final Integer quantity )
    {
        this.quantity = quantity;
    }


    public Long getId()
    {
        return id;
    }


    public void setId( final Long id )
    {
        this.id = id;
    }


    public User getUser()
    {
        return user;
    }


    public void setUser( final User user )
    {
        this.user = user;
    }


    public Product getProduct()
    {
        return product;
    }


    public void setProduct( final Product product )
    {
        this.product = product;
    }
}
