package com.mflewitt.controller;

import com.mflewitt.service.CartService;
import com.mflewitt.service.ProductService;
import com.mflewitt.service.UserService;
import java.security.Principal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@Controller
public class CartController
{
    @Autowired
    private CartService cartService;

    @Autowired
    private ProductService productService;

    @Autowired
    private UserService userService;

    @GetMapping( "/cart" )
    public String viewCart( Model model, Principal principal )
    {
        model.addAttribute( "total", cartService.getTotal( userService.findByUsername( principal.getName()) ) );
        model.addAttribute( "carts", cartService.getProductsinCart(userService.findByUsername( principal.getName())) );
        return "cart";
    }

    @GetMapping("/cart/{productid}")
    public String addToCart( @PathVariable Long productid, Principal principal )
    {
        cartService.addProduct( userService.findByUsername( principal.getName()), productService.findByID( productid ) );
        return "redirect:/shop";
    }

    @DeleteMapping("/cart/{id}")
    public String removeFromCart(@PathVariable Long id, Principal principal)
    {
        cartService.removeProduct( userService.findByUsername( principal.getName()), productService.findByID( id ) );

        return "redirect:/cart";
    }

    @GetMapping("/cart/{id}/add")
    public String UpQuantity(@PathVariable Long id, Principal principal)
    {
        cartService.increaseQuantity( userService.findByUsername( principal.getName()), productService.findByID( id ) );

        return "redirect:/cart";
    }

    @GetMapping("/cart/{id}/remove")
    public String DownQuantity(@PathVariable Long id, Principal principal)
    {
        cartService.decreaseQuality( userService.findByUsername( principal.getName()), productService.findByID( id ) );

        return "redirect:/cart";
    }

    @GetMapping("/cart/buynow")
    public String buyNow(Principal principal)
    {
        cartService.buyNow( userService.findByUsername( principal.getName()) );

        return "purchased";
    }
}
