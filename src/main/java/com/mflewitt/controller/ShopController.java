package com.mflewitt.controller;

import com.mflewitt.model.Product;
import com.mflewitt.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class ShopController
{
    @Autowired
    private ProductService productService;

    @GetMapping({"/", "/shop"})
    public String welcome( Model model) {
        model.addAttribute( "products", productService.findAll() );
        return "shop";
    }
}
