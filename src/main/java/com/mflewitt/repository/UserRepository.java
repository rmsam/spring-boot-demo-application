package com.mflewitt.repository;

import com.mflewitt.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long>
{
    User findByUsername( String username);

    User findByUsernameIsLike( String name );
}
