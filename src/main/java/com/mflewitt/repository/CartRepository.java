package com.mflewitt.repository;

import com.mflewitt.model.Cart;
import com.mflewitt.model.Product;
import com.mflewitt.model.User;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CartRepository extends JpaRepository<Cart, Long>
{
    List<Cart> findCartsByUserIs( User user);

    Cart findDistinctByUserIsAndProductIs(User user, Product product);
}
