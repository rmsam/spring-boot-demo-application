package com.mflewitt.repository;

import com.mflewitt.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductRepository extends JpaRepository<Product, Long>
{
    Product findByNameLike( String name );
}
